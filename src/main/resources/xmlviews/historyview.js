var data;

function generateViewByData(dataString) {
    data = JSON.parse(dataString);

    sum = 0;

    for (y=0;y<data.data.length;y++) {
        for (i=0;i<data.data[y].items.length;i++) {
            sum+=data.data[y].items[i].count*data.data[y].items[i].price;
        }
    }

    status = "-";
    if (data.status=="received") status="Заказ принят";
    if (data.status=="processed") status="Заказ обработан";
    if (data.status=="paid") status="Заказ оплачен";
    if (data.status=="canceled") status="Заказ отменен";

    $('#table').append($('<tbody>').append($('<tr>').append(
        $('<td>').append(data.id)).append(
        $('<td>').append(data.table)).append(
        $('<td>').append(sum+"руб.")).append(
        $('<td>').append(status)).append(
        $('<td>').append(data.createtime)).append(
        $('<td>').append('<button onclick="app.openDialog('+data.id+')">Подробнее</button>'))));

}


function clear() {
    $('#table').empty();
    $('#table').append("<thead><tr><td>№</td><td>Стол</td><td>Сумма</td><td>Статус</td><td>Дата</td><td></td></tr></thead>");

}


