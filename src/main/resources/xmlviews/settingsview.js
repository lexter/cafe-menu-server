function setServerPort(serverport) {
    $("#serverport").val(serverport);
}

function setMysqlHost(host) {
    $("#mysqlhost").val(host);
}

function setMysqlUser(user) {
    $("#mysqluser").val(user);
}

function setMysqlPassword(password) {
    $("#mysqlpassword").val(password);
}

function setMysqlBase(base) {
    $("#mysqlbase").val(base);
}

function setIpServer(ip) {
    $("#ipaddress").text(ip);
}

function save() {
    app.setServerPort($("#serverport").val());
    app.setMysqlHost($("#mysqlhost").val());
    app.setMysqlUser($("#mysqluser").val());
    app.setMysqlPassword($("#mysqlpassword").val());
    app.setMysqlBase($("#mysqlbase").val());
    app.closeDialog();
}


function clear() {
    $('.container').empty();
}


function findOrderList() {

}

