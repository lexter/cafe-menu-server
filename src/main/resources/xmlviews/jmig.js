(function($){
    jQuery.fn.migStart = function(){

        var mig = function() {
            var elm = this;
            this.timer = setInterval(function() {
                $(elm).toggleClass("mig");
            } , 200);

            $(elm).click(function(){
                $(elm).removeClass("mig");
                window.clearInterval(elm.timer);
            });
        };
        return this.each(mig);
    };
})(jQuery);