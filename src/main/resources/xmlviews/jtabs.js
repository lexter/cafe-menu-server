(function($){
    jQuery.fn.lightTabs = function(showTab,callback){

        var createTabs = function() {
            var tabs = this;
            i = 0;

            showPage = function(i,tabs){
                $(tabs).children("div").children("div").hide();
                $(tabs).children("div").children("div").eq(i).show();
                $(tabs).children("ul").children("li").removeClass("active");
                $(tabs).children("ul").children("li").eq(i).addClass("active");
            }

            showPage(showTab,tabs);

            $(tabs).children("ul").children("li").each(function(index, element){
                $(element).attr("data-page", i);
                i++;
            });

            $(tabs).children("ul").children("li").click(function(){
                showPage(parseInt($(this).attr("data-page")),tabs);
                callback();
            });
            callback();
        };
        return this.each(createTabs);
    };
})(jQuery);