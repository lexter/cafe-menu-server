var data;

function generateViewByData(dataString,num) {
    data = JSON.parse(dataString);

    ordersTab = $('<ul>');
    content = $('<div>');

    for (y=0;y<data.data.length;y++) {
        ordersTab.append($('<li>').append(y+1));

        orderdiv = $('<div>');
        table = $('<table>');
        sum = 0;
        for (i=0;i<data.data[y].items.length;i++) {

            table.append($('<tr>').append(
                $('<td>').append(data.data[y].items[i].name.rus)).append(
                $('<td>').append('<div style="max-width: 160px; mix-width: 100px; white-space: nowrap;"><button onclick="downItem('+data.data[y].num+','+data.data[y].items[i].id+')">-</button>'+data.data[y].items[i].count+'<button onclick="upItem('+data.data[y].num+','+data.data[y].items[i].id+')">+</button></div>')).append(
                $('<td>').append("x"+data.data[y].items[i].price+"руб.")).append(
                $('<td>').append('<button onclick="removeItem('+data.data[y].num+','+data.data[y].items[i].id+')">x</button>')));
            sum+=data.data[y].items[i].count*data.data[y].items[i].price;
        }
        if (data.data[y].items.length>0) table.append($('<tr>').append($('<td>').append("<b>Сумма:<b>")).append($('<td>')).append($('<td>')).append($('<td>').append("<b>"+sum+"руб.<b>")));
        else table.append($('<tr>').append($('<td>').append("<i>Заказ еще не составлен</i>")));

        orderdiv.append(table);
        content.append(orderdiv);
    }

    dom = $('<div id="order'+data.id+'">').append(
        $('<h2>Планшет №'+data.table+'</h2>')
    ).append(
        $('<div id="tab' + data.id + '" class="tabs">').append(
            ordersTab
        ).append(
            content
        )
    );

    $(".container").append(dom);
    $("#tab" + data.id).lightTabs(num-1,function() {});


    filterStatuses(data.status);
    if (data.status=="received") data.status="processed";

}

function filterStatuses(status) {
    $("#received").hide();
    $("#processed").hide();
    $("#paid").hide();
    $("#canceled").hide();

    if (status=="processed" || status=="received") {
        $("#paid").show();
        $("#canceled").show();
    }

    if (data.status =="received") $("#status").text("Заказ принят");
    if (data.status =="processed") $("#status").text("Заказ обработан");
    if (data.status =="paid") $("#status").text("Заказ оплачен");
    if (data.status =="canceled") $("#status").text("Заказ отменен");

}

function removeItem(num, id) {
    for (y=0;y<data.data.length;y++) {
        if (data.data[y].num == num) {
            for (i=0;i<data.data[y].items.length;i++) {
                if (data.data[y].items[i].id==id) {
                    data.data[y].items.splice(i,1);
                }
            }
        }
    }
    clear();
    generateViewByData(JSON.stringify(data),num);
}

function upItem(num, id) {
    for (y=0;y<data.data.length;y++) {
        if (data.data[y].num == num) {
            for (i=0;i<data.data[y].items.length;i++) {
                if (data.data[y].items[i].id==id) {
                    data.data[y].items[i].count++;
                    data.data[y].items[i].min_count=data.data[y].items[i].count;
                }
            }
        }
    }
    clear();
    generateViewByData(JSON.stringify(data),num);
}

function downItem(num, id) {
    for (y=0;y<data.data.length;y++) {
        if (data.data[y].num == num) {
            for (i=0;i<data.data[y].items.length;i++) {
                if (data.data[y].items[i].id==id) {
                    if (data.data[y].items[i].count>1) data.data[y].items[i].count--;
                    data.data[y].items[i].min_count=data.data[y].items[i].count;
                }
            }
        }
    }
    clear();
    generateViewByData(JSON.stringify(data),num);
}

function clear() {
    $('.container').empty();
}


