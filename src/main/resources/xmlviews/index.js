
function generateViewByData(data) {
    data = JSON.parse(data);

    ordersTab = $('<ul>');
    content = $('<div>');

    status = "";
    if (data.status=="received") {
        status = "ПРИНЯТ";
    } else if (data.status=="processed") {
        status = "ОБРАБОТАН";
    }

    for (y=0;y<data.data.length;y++) {
        ordersTab.append($('<li>').append(y+1));

        orderdiv = $('<div>');
        table = $('<table>');
        sum = 0;
        for (i=0;i<data.data[y].items.length;i++) {
            if (data.data[y].sent==null) continue;
            table.append($('<tr>').append($('<td>').append(data.data[y].items[i].name.rus)).append($('<td>').append(data.data[y].items[i].count+"x"+data.data[y].items[i].price+"руб.")));
            sum+=data.data[y].items[i].count*data.data[y].items[i].price;
        }
        if (data.data[y].items.length>0) table.append($('<tr>').append($('<td>').append("<b>Сумма:<b>")).append($('<td>').append("<b>"+sum+"руб.<b>")));
        else table.append($('<tr>').append($('<td>').append("<i>Заказ еще не составлен</i>")));


        orderdiv.append(table);
        content.append(orderdiv);
    }

    dom = $('<div id="order'+data.id+'" class="section card">').append(
        $('<h2>Стол №'+data.table+'</h2>')
    ).append(
        $('<div id="tab' + data.id + '" class="tabs">').append(
            ordersTab
        ).append(
            content
        )
    ).append(
        "Статус: "+status
    ).append(
        "</br>"
    ).append('<button onclick="app.openDialog('+data.id+');">Подробнее</button>');


    $(".container").append(dom);

    $("#tab" + data.id).lightTabs(0,function() {updateGrid();});
    if (data.status=="received") $(dom).migStart();

    grid.mount();

}


function update() {
    app.update();
}

function updateGrid() {
    grid = new Minigrid({
        container: '.cards',
        item: '.card',
        gutter: 6
    });
    grid.mount();
}

function clear() {
    $('.container').empty();
}

function findOrderList() {

}

