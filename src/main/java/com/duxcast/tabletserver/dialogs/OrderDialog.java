package com.duxcast.tabletserver.dialogs;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.server.Connection;
import com.duxcast.tabletserver.server.Server;
import com.duxcast.tabletserver.database.DataBase;

import dagger.Lazy;
import javafx.scene.web.WebEvent;
import org.json.JSONObject;

import javax.inject.Inject;
import java.util.Map;

public class OrderDialog extends Dialog {

    @Inject
    Lazy<Server> server;

    @Inject
    Lazy<MainDialog> main;

    @Inject
    Lazy<DataBase> dataBase;

    Order order;

//    public OrderDialog(int id) throws Exception {
//        super(600,600, "/xmlviews/orderview.html");
//        Global.components.inject(this);
//        orderlist = Order.findOrderById(dataBase.get(),id);
//        System.out.println("id");
//        System.out.println(orderlist.toJsonObject().toString());
//    }

    public OrderDialog(Order order) throws Exception {
        super(600,600, "/xmlviews/orderview.html");
        Global.components.inject(this);
        this.order = order;
        System.out.println("id");
        System.out.println(order.toJsonObject().toString());
    }

    public void save(String data) {
        try {
            JSONObject obj = new JSONObject(data);
//            Order order = Order.findOrderById(dataBase.get(), obj.optInt("id"));
            if (order == null || order.getStatus().equals(Order.Status.paid) || order.getStatus().equals(Order.Status.canceled)) {
                close();
                return;
            }

            int table = order.getTable();

            order.setData(obj.optJSONArray("data"));
            order.setStatus(obj.optString("status"));
            order.update();

            if (obj.optString("status").equals(Order.Status.paid.toString()) || obj.optString("status").equals(Order.Status.canceled.toString())) {
                for (Map.Entry<Integer,Order> entry : server.get().orders.entrySet()) {
                    if (entry.getValue()!=null && entry.getValue().getId()==obj.optInt("id")) {
                        server.get().orders.replace(entry.getKey(),Order.generateNewOrder(table));
                    }
                }
            } else {
                server.get().orders.replace(table,order);
            }


            JSONObject jsono = new JSONObject();
            jsono.put("action", "changeOrder");
            jsono.put("data", server.get().orders.get(obj.optInt("table")).getOrders());

            for (Connection connection:server.get().findSocketConnectionByTable(table)) {
                connection.sendMessage(jsono);
            }

            try {
                if (order.getStatus() == Order.Status.processed) {
                    jsono = new JSONObject();
                    jsono.put("action", "orderProcessed");
                    for (Connection connection : server.get().findSocketConnectionByTable(table)) {
                        connection.sendMessage(jsono);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            main.get().update();
            close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void closeDialog() {
        close();
    }
    
    public void log(String log) {
        System.err.println(log);
    }


    @Override
    public void handle(WebEvent<String> event) {
        if("command:ready".equals(event.getData())){
            windowObject.call("generateViewByData",order.toJsonObject().toString(),1);
        }
    }
}
