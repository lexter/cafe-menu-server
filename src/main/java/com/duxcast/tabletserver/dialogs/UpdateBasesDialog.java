package com.duxcast.tabletserver.dialogs;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.database.DataBase;
import com.duxcast.tabletserver.models.Item;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.server.Connection;
import com.duxcast.tabletserver.server.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.duxcast.tabletserver.utils.HttpCommand;
import com.duxcast.tabletserver.utils.SharedPref;
import dagger.Lazy;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;

public class UpdateBasesDialog extends Stage {

    @Inject
    Lazy<Server> server;

    @Inject
    Lazy<MainDialog> main;

    @Inject
    Lazy<DataBase> dataBase;
    
    public UpdateBasesDialog() throws IOException {
        Global.components.inject(this);

        initStyle(StageStyle.UTILITY);
        setAlwaysOnTop(true);
        
        Label label = new Label();
        label.setText("Обновление базы.");
        Insets insets = new Insets(10, 10, 10, 10);
        label.setPadding(insets);
        
        Scene scene = new Scene(label,270,50);
        setTitle("Tablet Server");
        setScene(scene);

        
        Platform.runLater(() -> new Thread(() -> {

            try {

                for (Map.Entry<Integer,Order> entry : server.get().orders.entrySet()) {
                    if (entry.getValue().getStatus().equals(Order.Status.processed) || entry.getValue().getStatus().equals(Order.Status.received)) {
                        Platform.runLater(() -> {
                            label.setText("Ошибка! Есть незавершенные заказы.");
                        });
                        Thread.sleep(4000);
                        Platform.runLater(() -> closeDialog());
                        return;
                    }
                }

                for (Map.Entry<Integer,Order> entry : server.get().orders.entrySet()) {
                    if (entry.getValue().getStatus().equals(Order.Status.empty)) {

                        JSONArray data = new JSONArray();
                        JSONObject emptyUser = new JSONObject();
                        emptyUser.put("num",1);
                        emptyUser.put("select",true);
                        emptyUser.put("items",new JSONArray());
                        data.put(emptyUser);

                        server.get().orders.get(entry.getValue().getTable()).setData(data);

                        JSONObject jsono = new JSONObject();
                        jsono.put("action", "changeOrder");
                        jsono.put("data", server.get().orders.get(entry.getValue().getTable()).getOrders());

                        for (Connection connection:server.get().findSocketConnectionByTable(entry.getValue().getTable())) {
                            connection.sendMessage(jsono);
                        }
                    }
                }

                final JSONArray jsonArray = new JSONArray(HttpCommand.executeForString("http://bohorest.ru/menucategories/getMenu.php"));
                Item.clearDataBase(dataBase.get());
                SharedPref.set("lastBaseUpdate",String.valueOf(System.currentTimeMillis()));
                for (int i=0;i<jsonArray.length();i++) {
                    final int inti = i;
                    new Item(jsonArray.optJSONObject(i)).save();
                    Platform.runLater(() -> {
                        label.setText("Позиции меню: " + inti + "/" + jsonArray.length());
                    });
                }

                final ArrayList<Item> items = Item.getAllItems(dataBase.get());
                Platform.runLater(() -> {
                    label.setText("Всего позиции в меню: " + items.size());
                });
                Thread.sleep(1000);
                Platform.runLater(() -> {
                    label.setText("Обновления планшетов");
                });
                server.get().updateTabletsBase();
                Thread.sleep(3000);
            } catch (Exception e) {
                e.printStackTrace();
            }

//
            Platform.runLater(() -> closeDialog());
        }).start());
        
        
    }

    public void openDialog(int id) {
        try {
            new OrderDialog(Order.findOrderById(dataBase.get(),id)).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void closeDialog() {
        close();
    }
    
    public void log(String log) {
        System.err.println(log);
    }
    
    
}
