/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duxcast.tabletserver.dialogs;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import netscape.javascript.JSObject;


public abstract class Dialog extends Stage implements EventHandler<WebEvent<String>> {

    JSObject windowObject;

    public Dialog(int height, int width, String htmlLayout) throws Exception {

        initStyle(StageStyle.UTILITY);
        setAlwaysOnTop(true);
       
        WebView webView = new WebView();
        webView.setContextMenuEnabled(false);
        WebEngine engine = webView.getEngine();
        engine.load(getClass().getResource(htmlLayout).toExternalForm());
        windowObject = (JSObject)webView.getEngine().executeScript("window");
        windowObject.setMember("app", this);

        engine.setOnAlert(this);
        
        Scene scene = new Scene(webView, height,width);
        setTitle("Tablet Server");
        setScene(scene);

    }

    @Override
    public abstract void handle(WebEvent<String> event);
}
