package com.duxcast.tabletserver.dialogs;

import java.io.IOException;

import com.duxcast.tabletserver.models.Order;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class AboutDialog extends Stage {

    public AboutDialog() throws IOException {
        initStyle(StageStyle.UTILITY);
        setAlwaysOnTop(true);
        
        Label label = new Label();
        label.setText("Разработано duxcast.com");
        Insets i = new Insets(10, 10, 10, 10);
        label.setPadding(i);
        
        Scene scene = new Scene(label,250,50);
        setTitle("Tablet Server");
        setScene(scene);

        
        Platform.runLater(() -> new Thread(() -> {
            try {
                Thread.sleep(8000);
            } catch(Exception e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> closeDialog());
        }).start());
        
        
    }
    
    public void closeDialog() {
        close();
    }
    
    public void log(String log) {
        System.err.println(log);
    }
    
    
}
