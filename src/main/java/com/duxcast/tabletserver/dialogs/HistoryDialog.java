package com.duxcast.tabletserver.dialogs;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.database.DataBase;

import java.util.ArrayList;

import dagger.Lazy;
import javafx.scene.web.WebEvent;

import javax.inject.Inject;

public class HistoryDialog extends Dialog {


    ArrayList<Order> orderlist;

    @Inject
    Lazy<DataBase> dataBase;

    public HistoryDialog() throws Exception {
        super(600,600, "/xmlviews/historyview.html");
        Global.components.inject(this);
        orderlist = Order.getOrderLastHungHistory(dataBase.get());
    }

    public void openDialog(int id) {
        try {
            new OrderDialog(Order.findOrderById(dataBase.get(),id)).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void closeDialog() {
        close();
    }
    
    public void log(String log) {
        System.err.println(log);
    }


    @Override
    public void handle(WebEvent<String> event) {
        if("command:ready".equals(event.getData())){
            windowObject.call("clear");
            for (Order ol:orderlist) windowObject.call("generateViewByData",ol.toJsonObject().toString());
        }
    }
}
