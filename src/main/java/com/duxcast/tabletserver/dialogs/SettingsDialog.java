package com.duxcast.tabletserver.dialogs;

import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.utils.SharedPref;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javafx.scene.web.WebEvent;


public class SettingsDialog extends Dialog {

    Order orderlist;
    
    public SettingsDialog() throws Exception {
        super(600,600, "/xmlviews/settingsview.html");
    }

    public void save(String data) {
        try {
            close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void closeDialog() {
        close();
    }
    
    public void log(String log) {
        System.err.println(log);
    }
    
    public void generateDataBase() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://"+SharedPref.get("mysqlhost", "127.0.0.1")+"/", SharedPref.get("mysqluser", "user"), SharedPref.get("mysqlpassword", "password"));
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE DATABASE IF NOT EXISTS `"+SharedPref.get("mysqlbase", "basename")+"` ");
            statement.close();
            connection.close();

            connection = DriverManager.getConnection("jdbc:mysql://"+SharedPref.get("mysqlhost", "127.0.0.1")+"/"+SharedPref.get("mysqlbase", "basename")+"?characterEncoding=utf8&autoReconnect=true", SharedPref.get("mysqluser", "user"), SharedPref.get("mysqlpassword", "password"));
            statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `orders` (`id` int(11) NOT NULL AUTO_INCREMENT," +
                    "`table` INT NOT NULL, " +
                    "`status` ENUM('empty','received','processed','paid','canceled','' ) NOT NULL, `data` text NOT NULL, `createtime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`))");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `items` (`id` int(11) NOT NULL AUTO_INCREMENT," +
                    "`hash` VARCHAR( 32 ) NOT NULL, "+
                    "`name` text NOT NULL, " +
                    "`preview_picture` text NOT NULL," +
                    "`detail_text` text NOT NULL," +
                    "`parent_id` INT NOT NULL," +
                    "`last_child` INT NOT NULL," +
                    "`name_lowcase` text NOT NULL," +
                    "`data` text NOT NULL," +
                    " `createtime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`))");


            statement.close();
            connection.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
                    
    public void setServerPort(String serverport) {
        SharedPref.set("serverport", serverport);
    }


    public void setMysqlHost(String host) {
        SharedPref.set("mysqlhost", host);
    }

    public void setMysqlUser(String user) {
        SharedPref.set("mysqluser", user);
    }

    public void setMysqlPassword(String password) {
        SharedPref.set("mysqlpassword", password);
    }

    public void setMysqlBase(String base) {
        SharedPref.set("mysqlbase", base);
    }

    @Override
    public void handle(WebEvent<String> event) {
        if("command:ready".equals(event.getData())){
            InetAddress currentIp = null;
            try {
                currentIp = InetAddress.getLocalHost();
            } catch (Exception e) {
                e.printStackTrace();
            }

            windowObject.call("setServerPort",SharedPref.get("serverport", "9999"));
            windowObject.call("setMysqlHost",SharedPref.get("mysqlhost", "127.0.0.1"));
            windowObject.call("setMysqlUser",SharedPref.get("mysqluser", "user"));
            windowObject.call("setMysqlPassword",SharedPref.get("mysqlpassword", "password"));
            windowObject.call("setMysqlBase",SharedPref.get("mysqlbase", "basename"));
            windowObject.call("setIpServer",currentIp.getHostAddress());
        }
    }
}
