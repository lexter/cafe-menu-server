package com.duxcast.tabletserver.dialogs;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.server.Server;
import com.duxcast.tabletserver.database.DataBase;
import dagger.Lazy;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import netscape.javascript.JSObject;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Map;

import static com.sun.tools.doclint.Entity.dagger;


public class MainDialog {

    @Inject
    Lazy<Server> server;

    @Inject
    Lazy<DataBase> dataBase;


    JSObject windowObject;

    public MainDialog() {
        Global.components.inject(this);
    }

    public void init(Stage stage) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/xmlviews/rootlayout.fxml"));
        Parent root = loader.load();
        stage.setTitle("Tablet Server");
        Scene scene = new Scene(root, 600,600);

        stage.setScene(scene);
        stage.initStyle(StageStyle.UTILITY);
        stage.setAlwaysOnTop(true);


        WebView webView = (WebView)root.lookup("#webview");
        webView.setContextMenuEnabled(false);
        WebEngine engine = webView.getEngine();
        engine.setJavaScriptEnabled(true);
        engine.load(getClass().getResource("/xmlviews/index.html").toExternalForm());
        windowObject = (JSObject) webView.getEngine().executeScript("window");
        windowObject.setMember("app", this);
        if (windowObject!=null) System.out.println("windowObject not null "+hashCode());

        engine.setOnAlert(event -> {
            if("command:ready".equals(event.getData())){
                update();
            }
        });


        if (dataBase==null) {
            try {
                new SettingsDialog().show();
                stage.close();
                return;
            } catch(Exception e) {
                e.printStackTrace();
            }
        } else {

        }

        stage.setOnCloseRequest(we -> System.exit(0));
        stage.show();
    }

    public JSObject getJsBridge() {
        if (windowObject==null) System.out.println("windowObject null "+hashCode());
        return windowObject;
    }

    @FXML
    public void settingsItem() {
        try {
            new SettingsDialog().show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    public void aboutApp() {
        try {
            new AboutDialog().show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    public void updateTabletsBases() {
        try {
            new UpdateBasesDialog().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void historyOrders() {
        try {
            new HistoryDialog().show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void openDialog(int id) {
        try {
            new OrderDialog(Order.findOrderById(dataBase.get(),id)).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized void update() {
        try {
            windowObject.call("clear");
            for (Map.Entry<Integer,Order> entry : server.get().orders.entrySet()) {
                if (!entry.getValue().getStatus().equals(Order.Status.empty)) windowObject.call("generateViewByData", entry.getValue().toJsonObject());
            }
            windowObject.call("updateGrid");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void log(String log) {
        System.err.println(log);
    }


}
