package com.duxcast.tabletserver.models;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.database.DataBase;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import dagger.Lazy;
import javafx.application.Platform;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;


public class Order {

    public enum Status {empty,received,processed,paid,canceled};

    private int id = -1;
    private int table;
    private JSONArray data;
    private Status status;
    private Date createtime;

    @Inject
    Lazy<DataBase> dataBase;


    public Order(JSONObject object) {
        Global.components.inject(this);
        this.id = object.optInt("id",-1);
        this.table = object.optInt("table",-1);
        this.status = Status.valueOf(object.optString("status"));
        this.data = object.optJSONArray("data");
        this.createtime = BDToDate(object.optString("createtime"));
    }
    
    public Order(int table, JSONArray array) {
        Global.components.inject(this);
        this.table = table;
        this.status = Status.empty;
        this.data = array;
        this.createtime = new Date();
    }
    
    public Order(ResultSet resultSet) {
        Global.components.inject(this);
        try {
            this.id = resultSet.getInt("id");
            this.table = resultSet.getInt("table");
            this.status = Status.valueOf(resultSet.getString("status"));
            this.data = new JSONArray(resultSet.getString("data"));
            this.createtime = BDToDate(resultSet.getString("createtime"));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getTable() {
        return table;
    }

    public JSONArray getOrders() {
        return this.data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setStatus(String status) {
        this.status = Status.valueOf(status);
    }

    public Status getStatus() {
        return status;
    }

    public void save() {
        try {
            String sql = "INSERT INTO `orders`(`table`,`status`,`data`) VALUES (?,?,?)";
            this.id = dataBase.get().executeUpdate(sql, table, status.toString(), data.toString());
            System.err.println("INSERT "+this.id+" "+status.toString()+" "+table);
         } catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    public void update() {
        try {
            String sql = "UPDATE `orders` SET `data`=?, `status`=? WHERE (`id`=? AND `table`=?)";
            dataBase.get().executeUpdate(sql, data.toString(), status.toString(), id, table);
            System.err.println("UPDATE "+id+" "+status.toString()+" "+table);
         } catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    public static Order generateNewOrder(int table) {
        try {
            JSONArray data = new JSONArray();
            JSONObject emptyUser = new JSONObject();
            emptyUser.put("num",1);
            emptyUser.put("select",true);
            emptyUser.put("items",new JSONArray());
            data.put(emptyUser);
            Order order = new Order(table, data);
            order.setStatus(Status.empty);
            return order;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Order findActualyOrderByTable(DataBase dataBase, int table) {
        try {
            String sql = "SELECT * FROM `orders` WHERE `table`=? AND `status`!=? AND `status`!=? LIMIT 0,1";
            ResultSet resultSet = dataBase.executeQuery(sql, table, "paid", "canceled");
            if (resultSet.next()) {
                return new Order(resultSet);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static Order findOrderById(DataBase dataBase, int id) {
        try {
            String sql = "SELECT * FROM `orders` WHERE `id`=? LIMIT 0,1";
            ResultSet resultSet = dataBase.executeQuery(sql, id);
            if (resultSet.next()) {
                return new Order(resultSet);
            }
         } catch(Exception e) {
            e.printStackTrace();
         }
        return null;
    }

    public static ArrayList<Order> getOrderLastHungHistory(DataBase dataBase) {
        ArrayList<Order> arrayList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM orders WHERE `status`=? OR `status`=? ORDER BY `createtime` DESC LIMIT 0,100 ";
            ResultSet resultSet = dataBase.executeQuery(sql,"paid","canceled");
            while (resultSet.next()) {
                arrayList.add(new Order(resultSet));
            }
         } catch(Exception e) {
            e.printStackTrace();
         }
        return arrayList;
    }

    public static HashMap<Integer,Order> findActualyOrders(DataBase dataBase) {
        HashMap<Integer,Order> orders = new HashMap<>();
        try {
            String sql = "SELECT * FROM `orders` WHERE `status`!=? AND `status`!=? LIMIT 0,1";
            ResultSet resultSet = dataBase.executeQuery(sql, "paid", "canceled");
            if (resultSet.next()) {
                Order order = new Order(resultSet);
                orders.put(order.getTable(),order);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return orders;
    }


    public JSONObject toJsonObject() {
        try {
            JSONObject object = new JSONObject();
            object.put("id", id);
            object.put("table", table);
            object.put("status", status.toString());
            object.put("data", data);
            object.put("createtime", DateToBD(createtime));
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String DateToBD(Date date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
            return dateFormat.format(date);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	
    public static Date BDToDate(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return dateFormat.parse(date);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //ORDER CHANGE

    public synchronized void addToList(JSONObject objectJs) {
        try {
            JSONArray jsonArray = getOrders();
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==objectJs.optInt("orderNum")) {
                    JSONArray items = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items.length(); j++) {
                        if (items.optJSONObject(j).optInt("id") == objectJs.optInt("itemId")) {
                            items.optJSONObject(j).put("count", items.optJSONObject(j).optInt("count") + 1);
                            jsonArray.optJSONObject(i).put("items",items);
                            setData(jsonArray);
                            update();
                            return;
                        }
                    }
                    JSONObject object = Item.getItemById(dataBase.get(),objectJs.optInt("itemId")).toJsonObject();
                    object.put("count", 1);
                    items.put(object);
                    jsonArray.optJSONObject(i).put("items",items);
                    setData(jsonArray);
                    update();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void removeFromList(JSONObject objectJs) {
        try {
            JSONArray jsonArray = getOrders();
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==objectJs.optInt("orderNum")) {
                    JSONArray items = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items.length(); j++) {
                        if (items.optJSONObject(j).optInt("id") == objectJs.optInt("itemId")) {
                            if (items.optJSONObject(j).optBoolean("sent",false)) {
                                if (items.optJSONObject(j).optInt("count")>items.optJSONObject(j).optInt("min_count",0)) {
                                    items.optJSONObject(j).put("count",items.optJSONObject(j).optInt("count")-1);
                                    jsonArray.optJSONObject(i).put("items",items);
                                    setData(jsonArray);
                                    update();
                                    return;
                                } else {
                                    return;
                                }
                            } else {
                                if (items.optJSONObject(j).optInt("count")>1) {
                                    items.optJSONObject(j).put("count",items.optJSONObject(j).optInt("count")-1);
                                    jsonArray.optJSONObject(i).put("items",items);
                                    setData(jsonArray);
                                    update();
                                    return;
                                } else {
                                    items.remove(j);
                                    jsonArray.optJSONObject(i).put("items",items);
                                    setData(jsonArray);
                                    update();
                                    return;
                                }
                            }
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void removeAllFromList(JSONObject objectJs) {
        try {
            JSONArray jsonArray = getOrders();
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==objectJs.optInt("orderNum")) {
                    JSONArray items = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items.length(); j++) {
                        if (items.optJSONObject(j).optInt("id") == objectJs.optInt("itemId")) {
                            items.remove(j);
                            jsonArray.optJSONObject(i).put("items",items);
                            setData(jsonArray);
                            update();
                            return;
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void addUser() {
        try {
            JSONArray jsonArray = getOrders();
            JSONObject object = new JSONObject();
            int count = jsonArray.length()+1;
            object.put("num",count);
            object.put("select",false);
            object.put("items",new JSONArray());
            jsonArray.put(object);
            setData(jsonArray);
            update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void clearList(JSONObject objectJs) {
        try {
            JSONArray jsonArray = getOrders();
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==objectJs.optInt("orderNum")) {
                    if (!jsonArray.optJSONObject(i).optBoolean("sent",false)) {
                        jsonArray.optJSONObject(i).remove("items");
                        jsonArray.optJSONObject(i).put("items", new JSONArray());
                    }
                }
            }
            setData(jsonArray);
            update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void setSentParamForThisOrder(JSONObject objectJs) {
        try {
            JSONArray jsonArray = getOrders();
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==objectJs.optInt("orderNum")) {
                    jsonArray.optJSONObject(i).put("sent", "true");
                    for (int j=0;j<jsonArray.optJSONObject(i).optJSONArray("items").length();j++) {
                        jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).put("min_count", jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).optInt("count", 0));
                        jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).put("sent", true);
                    }
                }
            }
            setData(jsonArray);
            setStatus(Order.Status.received);
            update();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void setSentParamForAllOrders() {
        try {
            JSONArray jsonArray = getOrders();
            for (int i=0;i<jsonArray.length();i++) {
                jsonArray.optJSONObject(i).put("sent", "true");
                for (int j=0;j<jsonArray.optJSONObject(i).optJSONArray("items").length();j++) {
                    jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).put("min_count", jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).optInt("count", 0));
                    jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).put("sent", true);
                }
            }
            setData(jsonArray);
            setStatus(Order.Status.received);
            update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
