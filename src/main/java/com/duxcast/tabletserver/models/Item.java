package com.duxcast.tabletserver.models;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.database.DataBase;
import com.duxcast.tabletserver.utils.TextUtils;
import dagger.Lazy;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.util.ArrayList;


public class Item {

    int id;
    String hash;
    JSONObject name = new JSONObject();
    String preview_picture = null;
    JSONObject detail_text = new JSONObject();
    JSONObject data = new JSONObject();
    int parent_id = -1;
    ArrayList<Item> subItems = new ArrayList<Item>();

    @Inject
    Lazy<DataBase> dataBase;

    public Item(JSONObject jsonObject) {
        Global.components.inject(this);
        id = jsonObject.optInt("id");

        name = jsonObject.optJSONObject("name");

        hash = jsonObject.optString("hash");

        detail_text = jsonObject.optJSONObject("detail_text");

        preview_picture = jsonObject.optString("preview_picture");
        if (TextUtils.isEmpty(preview_picture)) preview_picture= jsonObject.optString("detail_picture");
        if (!preview_picture.contains("http://bohorest.ru")) preview_picture="http://bohorest.ru"+preview_picture;


        data = new JSONObject();
        if (jsonObject.optDouble("price",-1)!=-1) {
            try {
                data.put("price",(int)jsonObject.optDouble("price"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            data.put("last_child",jsonObject.optBoolean("last_child", false));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            data.put("prop",jsonObject.optJSONArray("prop"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            data.put("sent",jsonObject.optBoolean("sent", false));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            data.put("min_count",jsonObject.optInt("min_count", 0));
        } catch (Exception e) {
            e.printStackTrace();
        }

        parent_id = jsonObject.optInt("parent_id");
        JSONArray jsonArray = jsonObject.optJSONArray("data");
        if (jsonArray==null) return;
        for (int i=0;i<jsonArray.length();i++) {
            subItems.add(new Item(jsonArray.optJSONObject(i)));
        }

    }

    public Item(ResultSet resultSet) {
        Global.components.inject(this);
        try {
            id = resultSet.getInt("id");
            hash = resultSet.getString("hash");
            try {name = new JSONObject(resultSet.getString("name"));} catch (Exception ignored) {}
            preview_picture = resultSet.getString("preview_picture");
            try {detail_text = new JSONObject(resultSet.getString("detail_text"));} catch (Exception ignored) {};
            data = new JSONObject(resultSet.getString("data"));
            parent_id = resultSet.getInt("parent_id");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public int save() {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            if (name!=null) {
                stringBuilder.append(name.optString("rus", ""));
                stringBuilder.append(name.optString("eng", ""));
            }
            if (detail_text!=null) {
                stringBuilder.append(detail_text.optString("rus", ""));
                stringBuilder.append(detail_text.optString("eng", ""));
            }

            if (id!=0) {
                String sql = "INSERT INTO `items`(`id`,`hash`,`name`,`preview_picture`,`detail_text`,`data`,`parent_id`,`last_child`,`name_lowcase`) VALUES (?,?,?,?,?,?,?,?,?)";
                dataBase.get().executeUpdate(sql,id,hash,name.toString(),preview_picture,(detail_text!=null)?detail_text.toString():"",data.toString(),parent_id,(data.optBoolean("last_child",false))?1:0,stringBuilder.toString().toLowerCase());
            } else {
                String sql = "INSERT INTO `items`(`hash`,`name`,`preview_picture`,`detail_text`,`data`,`parent_id`,`last_child`,`name_lowcase`) VALUES (?,?,?,?,?,?,?,?)";
                id = dataBase.get().executeUpdate(sql,hash,name.toString(),preview_picture,(detail_text!=null)?detail_text.toString():"",data.toString(),parent_id,(data.optBoolean("last_child",false))?1:0,stringBuilder.toString().toLowerCase());
            }

            for (Item item:subItems) {
                item.parent_id = id;
                item.save();
            }

            return id;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static Item getItemById(DataBase dataBase, int id) {
        try {
            ResultSet resultSet = dataBase.executeQuery("SELECT * FROM `items` WHERE id = ? LIMIT 0,1", id);
            if (resultSet.next()) {
                return new Item(resultSet);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Item> getAllItems(DataBase dataBase) {

        ArrayList<Item> items = new ArrayList<>();
        try {
            ResultSet resultSet = dataBase.executeQuery("SELECT * FROM `items` WHERE 1");
            while (resultSet.next()) {
                items.add(new Item(resultSet));
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            return items;
        }
    }


    public static void clearDataBase(DataBase dataBase) {
        dataBase.executeUpdate("TRUNCATE TABLE `items`");
    }

    public JSONObject toJsonObject() {
        try {
            JSONObject object = new JSONObject();
            object.put("id", id);
            object.put("hash", hash);
            object.put("name", name);
            object.put("preview_picture", preview_picture);
            object.put("detail_text", detail_text);

            object.put("parent_id",parent_id);

            object.put("price",data.optInt("price"));
            object.put("prop",data.optJSONArray("prop"));
            object.put("sent",data.optBoolean("sent", false));

            object.put("min_count",data.optInt("min_count", 0));

            object.put("last_child",data.optBoolean("last_child", false));

            JSONArray jsonArray = new JSONArray();
            for (Item item:subItems) {
                jsonArray.put(item.toJsonObject());
            }
            object.put("data", jsonArray);

            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
