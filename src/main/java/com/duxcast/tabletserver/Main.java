package com.duxcast.tabletserver;

import com.duxcast.tabletserver.dagger.DaggerComponents;
import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.dialogs.MainDialog;
import com.duxcast.tabletserver.server.Server;
import dagger.Lazy;
import javafx.application.Application;

import javafx.application.Platform;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.io.IOException;

public class Main extends Application {

    @Inject
    Lazy<Server> server;

    @Inject
    Lazy<MainDialog> mainDialog;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        Global.components = DaggerComponents.create();
        Global.components.inject(this);

        server.get().initSocketThread();
        Platform.runLater(() -> {
            try {
                mainDialog.get().init(primaryStage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }


    public static void main(String[] args) {
        launch(args);
    }
    
}
