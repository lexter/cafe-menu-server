package com.duxcast.tabletserver.database;

import com.duxcast.tabletserver.utils.SharedPref;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class DataBase {

    Connection connection;
    Statement statement;
    String host = "localhost";
    String database = "boho";
    String user = "root";
    String password = "toor";

    public DataBase() {
        System.out.println("DataBase start!");
        host = SharedPref.get("mysqlhost", "127.0.0.1");
        database = SharedPref.get("mysqlbase", "");
        user = SharedPref.get("mysqluser", user);
        password = SharedPref.get("mysqlpassword", password);
        String url = "jdbc:mysql://"+host+"/"+database+"?characterEncoding=utf8&autoReconnect=true";
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ResultSet executeQuery(String query, Object... params) {
        synchronized (connection) {
            try {
                PreparedStatement prstatement = (PreparedStatement) connection.prepareStatement(query);
                for (int i = 0; i < params.length; i++) {
                    if (params[i] instanceof String) {
                        prstatement.setString((i + 1), (String) params[i]);
                    }
                    if (params[i] instanceof Integer) {
                        prstatement.setInt((i + 1), (Integer) params[i]);
                    }
                    if (params[i] instanceof Boolean) {
                        prstatement.setBoolean((i + 1), (Boolean) params[i]);
                    }
                }
                ;
                return prstatement.executeQuery();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    } 

    
    public int executeUpdate(String query, Object... params) {
        synchronized (connection) {
            try {
                PreparedStatement prstatement = (PreparedStatement) connection.prepareStatement(query);
                for (int i = 0; i < params.length; i++) {
                    if (params[i] instanceof String) {
                        prstatement.setString((i + 1), (String) params[i]);
                    }
                    if (params[i] instanceof Integer) {
                        prstatement.setInt((i + 1), (Integer) params[i]);
                    }
                    if (params[i] instanceof Boolean) {
                        prstatement.setBoolean((i + 1), (Boolean) params[i]);
                    }
                }
                prstatement.executeUpdate();
                int id = -1;
                ResultSet resultSet = executeQuery("SELECT LAST_INSERT_ID()");
                if (resultSet.next()) {
                    id = resultSet.getInt(1);
                }
                return id;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        }
    }

    
    public void close() {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }



    private void printResults(ResultSet rs) throws SQLException {
        String id, name, beside;
        while (rs.next()) {
            id = rs.getString("id");
            name = rs.getString("name");
            beside = rs.getString("beside");
            System.out.println("******************************");
            System.out.println("id: " + id);
            System.out.println("name: " + name);
            System.out.println("beside: " + beside);
            System.out.println("******************************");
        }
    }
     

}
