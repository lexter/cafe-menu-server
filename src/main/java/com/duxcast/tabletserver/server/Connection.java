package com.duxcast.tabletserver.server;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.dialogs.MainDialog;
import com.duxcast.tabletserver.models.Item;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.database.DataBase;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import com.duxcast.tabletserver.utils.SharedPref;
import com.duxcast.tabletserver.utils.SoundProcessor;
import com.sun.istack.internal.Nullable;
import dagger.Lazy;
import javafx.application.Platform;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

public class Connection implements Runnable {

    private Socket socket;

    private String tabletIp;

    private BufferedInputStream inputStream;
    private BufferedOutputStream outputStream;

    @Inject
    Lazy<Server> server;

    @Inject
    Lazy<MainDialog> main;

    @Inject
    Lazy<DataBase> dataBase;

    @Inject
    Lazy<SoundProcessor> soundProcessor;

    @Nullable
    private Order order;
        
	public Connection() throws JSONException {
        Global.components.inject(this);
	}

    public String getTabletIp() {
        return tabletIp;
    }

    public Connection setSocket(Socket socket) throws IOException {
        this.socket = socket;

        tabletIp = socket.getInetAddress().toString();

        inputStream = new BufferedInputStream(socket.getInputStream());
        outputStream = new BufferedOutputStream(socket.getOutputStream());

        return this;
    }


	void closeConnection() {
        try {
            if (socket != null)
                socket.close();
            System.out.println("closeConnection()");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public synchronized void sendMessage(JSONObject message) throws IOException {
        if (message!=null && socket.isConnected()) {
            Frame frame = new Frame(message.toString());
            frame.send(outputStream);
            outputStream.flush();
            System.out.println("-> "+message.optString("action")+" *** "+getTabletIp()+" *** "+message.toString());
        }
	}

	@Override
	public void run() {
        try {

            while (socket.isConnected()) {
                try {
                    Frame frame;
                    try {
                        frame = new Frame(inputStream);
                    } catch (IOException e) {
                        break;
                    }

                    JSONObject js;
                    try {
                        js = new JSONObject(frame.getData());
                    } catch (Exception e) {
                        continue;
                    }

                    System.out.println("<- "+js.optString("action")+" *** "+getTabletIp()+" *** "+js.toString());

                    if (js.optString("action").equals("updateOrder")) {
                        updateOrders(js);
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("addItem")) {
                        prepareOrder(js);
                        order.addToList(js); //int itemId, int orderNum
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("removeItem")) {
                        prepareOrder(js);
                        order.removeFromList(js); //int itemId, int orderNum
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("removeAllItems")) {
                        prepareOrder(js);
                        order.removeAllFromList(js); //int itemId, int orderNum
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("addUser")) {
                        prepareOrder(js);
                        order.addUser();
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("clearList")) {
                        prepareOrder(js);
                        order.clearList(js); //int orderNum
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("acceptOrder")) {
                        prepareOrder(js);
                        order.setSentParamForThisOrder(js); //int orderNum
                        Platform.runLater(() -> {
                            try {
                                main.get().update();
                                soundProcessor.get().ding();
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                        });
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        JSONObject jsono = new JSONObject();
                        jsono.put("action", "orderSeccess");
                        sendMessage(jsono);
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("acceptAllOrders")) {
                        prepareOrder(js);
                        order.setSentParamForAllOrders();
                        Platform.runLater(() -> {
                            try {
                                main.get().update();
                                soundProcessor.get().ding();
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                        });
                        for (Map.Entry<String,Connection> entry : server.get().connections.entrySet()) {
                            if (entry.getValue().getOrder().getTable()==getOrder().getTable()) {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("action", "changeOrder");
                                jSONObject.put("data", order.getOrders());
                                entry.getValue().sendMessage(jSONObject);
                            }
                        }
                        JSONObject jsono = new JSONObject();
                        jsono.put("action", "orderSeccess");
                        sendMessage(jsono);
                        Platform.runLater(() -> main.get().update());
                    }

                    if (js.optString("action").equals("checkLastBaseUpdate")) {
                        sendLastDatabaseUpdate();
                    }

                    if (js.optString("action").equals("updateBase")) {
                        updateBase();
                    }

                    if (js.optString("action").equals("setTable")) {
                        setTable(js.optInt("table"));
                    }

                    if (js.optString("action").equals("bootComplete")) {
                        //
                    }
                    if (js.optString("action").equals("reboot")) {
                        break;
                    }
                    if (js.optString("action").equals("shutdown")) {
                        break;
                    }
                } catch(IOException e) {
                    e.printStackTrace();
                    break;
                }
            }

            closeConnection();
            System.out.println("ENDLOOP");
        } catch (Exception e) {
            e.printStackTrace();
            closeConnection();
            System.out.println("EXCEPTION");
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    public Order getOrder() {
        return order;
    }

    public void setTable(int table) {
        try {
            order = server.get().findOrderByTable(table);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action", "changeOrder");
            jSONObject.put("data", order.getOrders());
            sendMessage(jSONObject);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void updateOrders(JSONObject js) {
        prepareOrder(js.optInt("table",-1));
        order.setData(js.optJSONArray("data"));
        order.setStatus(Order.Status.processed);
        order.update();
    }

    private void prepareOrder(JSONObject obj) {
        order = server.get().findOrderByTable(obj.optInt("table",-1));
    }

    private Connection prepareOrder(int table) {
        order = server.get().findOrderByTable(table);
        return this;
    }

    private void endOrders(JSONObject js) {
        if (order.getId() == -1) order.save();
        order.setData(js.optJSONArray("data"));
        order.setStatus(Order.Status.received);
        order.update();

        Platform.runLater(() -> {
            try {
                main.get().update();
                soundProcessor.get().ding();
            } catch(Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void sendLastDatabaseUpdate() {
        try {

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action", "lastBaseUpdate");
            jSONObject.put("date", SharedPref.get("lastBaseUpdate","1"));
            sendMessage(jSONObject);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void updateBase() {
        try {

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action", "updateBase");
            jSONObject.put("date", SharedPref.get("lastBaseUpdate","1"));


            ArrayList<Item> items = Item.getAllItems(dataBase.get());
            JSONArray jsonArray = new JSONArray();
            for (Item item:items) jsonArray.put(item.toJsonObject());

            jSONObject.put("data", jsonArray);

            sendMessage(jSONObject);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

//    public Connection refreshOrder() {
//        order = Order.findActualyOrderByTable(dataBase.get(), order.getTable());
//        return this;
//    }
}
