package com.duxcast.tabletserver.server;

import com.duxcast.tabletserver.dagger.Global;
import com.duxcast.tabletserver.database.DataBase;
import com.duxcast.tabletserver.dialogs.MainDialog;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.utils.SharedPref;
import dagger.Lazy;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Server {

    public int port = 9999;

    public ExecutorService service = Executors.newCachedThreadPool();
    public HashMap<String,Connection> connections;
    public HashMap<Integer,Order> orders;

    Thread socThread;

    @Inject
    Lazy<MainDialog> main;

    @Inject
    Lazy<DataBase> dataBase;

    public Server() {
        System.out.println("Server start! "+this.hashCode());
        Global.components.inject(this);
        connections = new HashMap<>();
        orders = Order.findActualyOrders(dataBase.get());
    }

    public void initSocketThread() {
        socThread = new Thread(() -> {
            port = Integer.valueOf(SharedPref.get("serverport", "9999"));

            try (ServerSocket listenSocket = new ServerSocket(port)) {
                listenSocket.setPerformancePreferences(1000, 1000, 1000);
                while (!listenSocket.isClosed()) {
                    System.out.println("Waiting connect...");
                    Socket socket = listenSocket.accept();
                    socket.setPerformancePreferences(1000, 1000, 1000);
                    System.out.println("New connect "+socket.getInetAddress().toString());

                    Connection tablet = new Connection().setSocket(socket);
                    connections.put(socket.getInetAddress().toString(),tablet);

                    service.submit(tablet);
                    if (service instanceof ThreadPoolExecutor) {
                        System.out.println("Pool size is now "+((ThreadPoolExecutor) service).getActiveCount());
                        System.out.println(connections.size());
                        for (Map.Entry<String,Connection> entry : connections.entrySet()) {
                            System.out.println(entry.getKey()+" = "+entry.getValue());
                        }
                    }

                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                System.err.println(ioe.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            socThread = null;
        });
        socThread.start();
        
    }
    
    public void updateTabletsBase() {
        try {
            for (Map.Entry<String,Connection> entry : connections.entrySet()) {
                entry.getValue().sendLastDatabaseUpdate();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Connection> findSocketConnectionByTable(int table) {
        ArrayList<Connection> conns = new ArrayList<>();
        for (Map.Entry<String,Connection> entry : connections.entrySet()) {
            if (entry.getValue().getOrder()!=null && entry.getValue().getOrder().getTable()==table) {
                conns.add(entry.getValue());
            }
        }
        return conns;
    }

    public synchronized Order findOrderByTable(int table) {
        Order order = null;
        for (Map.Entry<Integer,Order> entry : orders.entrySet()) {
            if (entry.getKey()==table) order = entry.getValue();
        }

        if (order == null) order = Order.findActualyOrderByTable(dataBase.get(),table);
        if (order == null) order = Order.generateNewOrder(table);
        if (order != null && order.getId() == -1) {
            order.save();
        }

        if (order == null) return null;
        orders.put(table,order);
        return order;
    }

}
