package com.duxcast.tabletserver.utils;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;


public class SoundProcessor {

    MediaPlayer soundDing,soundPamp;

    public SoundProcessor() {
        soundDing = new MediaPlayer(new Media(new File("ding.wav").toURI().toString()));
        soundPamp = new MediaPlayer(new Media(new File("pamp.wav").toURI().toString()));
    }

    public synchronized void ding() {
        soundDing.stop();
        soundDing.play();
    }

    public synchronized void pamp() {
        soundDing.stop();
        soundPamp.play();
    }

}
