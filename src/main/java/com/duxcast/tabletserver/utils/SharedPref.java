/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duxcast.tabletserver.utils;

import com.duxcast.tabletserver.Main;
import java.util.prefs.Preferences;
/**
 *
 * @author admin
 */
public class SharedPref {
    
    
    public static String get(String param,String defValue) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        return prefs.get(param, defValue);
    }

    public static void set(String param,String value) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        prefs.put(param, value);
    }
    
    public static void remove(String param) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        prefs.remove(param);
    }
    
}
