package com.duxcast.tabletserver.utils;


import okhttp3.*;

public class HttpCommand {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static String executeForString(String url) throws Exception {

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
//                .add("email", "Jurassic@Park.com")
//                .add("tel", "90301171XX")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


 }
