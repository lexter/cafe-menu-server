package com.duxcast.tabletserver.dagger;

import com.duxcast.tabletserver.Main;
import com.duxcast.tabletserver.dialogs.*;
import com.duxcast.tabletserver.models.Item;
import com.duxcast.tabletserver.models.Order;
import com.duxcast.tabletserver.server.Server;
import com.duxcast.tabletserver.server.Connection;
import dagger.Component;

import javax.inject.Singleton;


@Singleton
@Component(modules={Modules.class})
public interface Components {
    void inject(Main m);
    void inject(OrderDialog od);
    void inject(UpdateBasesDialog ubd);
    void inject(MainDialog m);
    void inject(Connection c);
    void inject(HistoryDialog hd);
    void inject(Server server);
    void inject(Item item);
    void inject(Order order);
}