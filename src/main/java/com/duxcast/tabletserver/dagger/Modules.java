package com.duxcast.tabletserver.dagger;

import com.duxcast.tabletserver.dialogs.MainDialog;
import com.duxcast.tabletserver.server.Server;
import com.duxcast.tabletserver.database.DataBase;
import com.duxcast.tabletserver.utils.SoundProcessor;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;


@Module
public class Modules {

    @Provides @Singleton
    Server provideServer() {
        return new Server();
    }

    @Provides @Singleton
    MainDialog provideMainDialog() {
        return new MainDialog();
    }

    @Provides @Singleton
    DataBase provideDataBase() {
        return new DataBase();
    }

    @Provides @Singleton
    SoundProcessor provideSoundProcessor() {
        return new SoundProcessor();
    }
}
